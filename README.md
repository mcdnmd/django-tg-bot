# Template for backend course

В качестве библиотеки для работы с телеграмом используется `python-telegram-bot`.

СУБД - `db.sqlite3`

## Запуск

Перед запуском нужно создать `.env` в `src/app/config` по примеру `.env.example`

Установка зависимостей
```bash
pipenv sync
```

Запуск телеграм бота
```bash
make command c=run_bot
```

Запуск вебапки
```bash
make dev
```