from django.core.management import BaseCommand

from app.internal import bot


class Command(BaseCommand):
    help = "Run telegram bot"

    def handle(self, *args, **options):
        bot.setup_bot()
