from django.contrib import admin

from app.internal.models.telegram_user import TelegramUser


@admin.register(TelegramUser)
class TelegremUserAdmin(admin.ModelAdmin):
    list_display = ("id", "telegram_id", "username", "phone")
