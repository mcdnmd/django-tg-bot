import uuid

from django.db import models


class TelegramUser(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    telegram_id = models.PositiveIntegerField("telegram_id", null=False, unique=True)
    username = models.CharField("username", max_length=32, null=False, default="")
    phone = models.CharField("phone", max_length=16, null=True)

    created_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField(auto_now=True)
