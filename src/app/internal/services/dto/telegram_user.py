from pydantic import BaseModel


class CreateTelegramUserDto(BaseModel):
    telegram_id: int
    username: str | None


class UpdateTelegramUserDto(BaseModel):
    telegram_id: int
    username: str | None
    phone: str | None
