import uuid

from app.internal.models.telegram_user import TelegramUser
from app.internal.services.dto.telegram_user import CreateTelegramUserDto, UpdateTelegramUserDto


class TelegramUserRepository:
    @staticmethod
    def get(user_id: uuid.UUID) -> None | TelegramUser:
        return TelegramUser.objects.filter(id=user_id).first()

    @staticmethod
    def get_by_tg_id(telegram_id: int) -> None | TelegramUser:
        return TelegramUser.objects.filter(telegram_id=telegram_id).first()

    @staticmethod
    def create(telegram_dto: CreateTelegramUserDto) -> TelegramUser:
        user, created = TelegramUser.objects.get_or_create(
            telegram_id=telegram_dto.telegram_id, username=telegram_dto.username
        )
        return user

    @staticmethod
    def update(telegram_dto: UpdateTelegramUserDto) -> None:
        user = TelegramUser.objects.filter(telegram_id=telegram_dto.telegram_id).first()
        if not user:
            raise ValueError("Telegram user with telegram_id=%s is not exist." % telegram_dto.telegram_id)
        TelegramUser.objects.update(**telegram_dto.dict(exclude={"telegram_id"}, exclude_none=True))

    def phone_provided(self, telegram_id: int) -> bool:
        user = self.get_by_tg_id(telegram_id)
        if user:
            return True if user.phone else False
        return False
