from django.conf import settings
from telegram.ext import Application, CommandHandler, MessageHandler, filters

from app.internal.transport.bot import handlers


def setup_bot():
    application = Application.builder().token(settings.BOT_TOKEN).build()
    application.add_handler(CommandHandler("start", handlers.cmd_start))
    application.add_handler(CommandHandler("me", handlers.cmd_me))
    application.add_handler(CommandHandler("set_phone", handlers.cmd_send_phone))
    application.add_handler(MessageHandler(filters.CONTACT, handlers.handle_contact))

    application.run_polling()
