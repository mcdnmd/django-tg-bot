import uuid

from django.http import JsonResponse

from app.internal.services.telegram_user_repository import TelegramUserRepository


def me(requesr, id) -> JsonResponse:
    telegram_user_repo = TelegramUserRepository()

    try:
        user_id = uuid.UUID(id)
    except ValueError:
        return JsonResponse(data={"id": id, "message": "User in not exist."}, status=404)
    user = telegram_user_repo.get(user_id)

    if user is None:
        return JsonResponse(data={"id": user_id, "message": "User in not exist."}, status=404)

    return JsonResponse(data={"id": user.id, "name": user.username, "phone_number": user.phone}, status=200)
