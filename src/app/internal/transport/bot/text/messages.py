HELLO_MESSAGE = "Hello from this bot 😁"
COMMAND_REQUIRED_PHONE = "Please provide a phone using `/set_phone` command, after that you will have full access"
REQUEST_ENTER_PHONE = "Press button send phone"
THX_PHONE_PROVIDED = "Thank for your contact"
TELEGRAM_USER_PREVIEW = (
    "🤖All about you:\n\nID:\t{id}\nTelegramId:\t{telegram_id}\nUserName:\t{username}\n"
    "Phone:\t{phone}\nCreatedAt:\t{created_at}\nUpdatedAt:\t{updated_at}"
)
