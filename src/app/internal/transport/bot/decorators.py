from asgiref.sync import sync_to_async
from telegram import Update
from telegram.ext import ContextTypes

from app.internal.services.telegram_user_repository import TelegramUserRepository
from app.internal.transport.bot.text.messages import COMMAND_REQUIRED_PHONE


def phone_required(function: callable) -> callable:
    telegram_user_repo = TelegramUserRepository()

    async def wrap(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
        has_phone = await sync_to_async(telegram_user_repo.phone_provided)(update.message.from_user.id)
        if has_phone:
            await function(update, context)
        else:
            await context.bot.send_message(chat_id=update.message.chat_id, text=COMMAND_REQUIRED_PHONE)

    return wrap
