from asgiref.sync import sync_to_async
from telegram import KeyboardButton, ReplyKeyboardMarkup, Update
from telegram.ext import ContextTypes

from app.internal.services.dto.telegram_user import CreateTelegramUserDto, UpdateTelegramUserDto
from app.internal.services.telegram_user_repository import TelegramUserRepository
from app.internal.transport.bot.decorators import phone_required
from app.internal.transport.bot.text import messages

telegram_user_repo = TelegramUserRepository()


async def cmd_start(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    await sync_to_async(telegram_user_repo.create)(
        CreateTelegramUserDto(telegram_id=update.message.from_user.id, username=update.message.from_user.username)
    )
    await update.effective_message.reply_text(text=messages.HELLO_MESSAGE)


async def cmd_send_phone(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    await update.message.reply_text(
        text=messages.REQUEST_ENTER_PHONE,
        reply_markup=ReplyKeyboardMarkup(
            [[KeyboardButton(text="Отправить телефон", request_contact=True)]], one_time_keyboard=True
        ),
    )


async def handle_contact(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    contact = update.effective_message.contact
    phone = contact.phone_number
    await sync_to_async(telegram_user_repo.update)(
        UpdateTelegramUserDto(telegram_id=update.effective_message.chat_id, phone=phone)
    )
    await update.message.reply_text(text=messages.THX_PHONE_PROVIDED, reply_markup=None)


@phone_required
async def cmd_me(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    user = await sync_to_async(telegram_user_repo.get_by_tg_id)(update.message.from_user.id)
    await update.message.reply_text(
        text=messages.TELEGRAM_USER_PREVIEW.format(
            id=user.id,
            telegram_id=user.telegram_id,
            username=user.username,
            phone=user.phone,
            created_at=user.created_at,
            updated_at=user.update_at,
        )
    )
