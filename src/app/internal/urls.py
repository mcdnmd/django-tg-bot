from django.urls import path

from app.internal.transport.rest import handlers

urlpatterns = [path("me/<str:id>", handlers.me)]
